/****************************************************************************
 * examples/BMA456/mpu9250_example.c
 *
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <nuttx/sensors/mpu9250.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int MPU9250_main(int argc, char *argv[])
#endif
{
	int fd,ret;
	long arg = 0;
	char buffer[2];

	buffer[0] = 0x75;
	buffer[1] = 0x17;
	buffer[2] = 0x18;

	arg =  (long)&buffer[0];
	printf("Adress buffer: %i\n", &buffer[0]);
	printf("Adress in long: %i\n",arg);


	printf("example_mpu9250 start\n");

	fd = open("/dev/MPU9250_1",O_RDWR);
	printf("Return open: %i\n", &fd);

	/*first read*/
	ret = ioctl(fd,1,arg);

	printf("buffer [0] in main: %x \n", buffer[0]);
	printf("buffer [1] in main: %x \n", buffer[1]);
	printf("buffer [2] in main: %x \n", buffer[2]);


	ret = close(fd);
	printf("Return close: %i\n", ret);
	printf("example_mpu9250 end\n");




	return 0;
}
