/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Fix.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/datatype/Fix.h"

namespace uavcan {

Fix::Fix(gnss_fix_s &fix_data)
    : Data(
        42 + 2 * fix_data.sizePositionCovariance
            + 2 * fix_data.sizeVelocityCovariance, SIGNATURE, SERVICE,
        DATATYPEID)
  {
    setTime(fix_data.timestamp, fix_data.gnss_timestamp,
        fix_data.gnss_time_standard, fix_data.num_leap_seconds);
    setCoordinates(fix_data.longitude_deg_1e8, fix_data.latitude_deg_1e8);
    setHeight(fix_data.height_ellipsoid_mm, fix_data.height_msl_mm);
    setVelocity(fix_data.ned_velocity);
    setGPSInfo(fix_data.sats_used, fix_data.status, fix_data.pdop);
    setPositionCovariance(fix_data.positionCovariance,
        fix_data.sizePositionCovariance);
    setVelocityCovariance(fix_data.velocityCovariance,
        fix_data.sizeVelocityCovariance, fix_data.sizePositionCovariance);

  }

void Fix::setTime(uint64_t timestamp, uint64_t gnss_timestamp,
    GNSS_TIME_STANDARD gnss_time_standard, uint8_t num_leap_seconds)
  {
    Data::setData((void*) &timestamp, 0, 56);
    Data::setData((void*) &gnss_timestamp, 56, 56);
    Data::setData((void*) &gnss_time_standard, 112, 3);
    Data::setData((void*) &num_leap_seconds, 120, 8);
  }

void Fix::setCoordinates(int64_t longitude_deg_1e8, int64_t latitude_deg_1e8)
  {
    Data::setData((void*) &longitude_deg_1e8, 128, 37);
    Data::setData((void*) &latitude_deg_1e8, 165, 37);
  }

void Fix::setHeight(int32_t height_ellipsoid_mm, int32_t height_msl_mm)
  {
    Data::setData((void*) &height_ellipsoid_mm, 202, 27);
    Data::setData((void*) &height_msl_mm, 229, 27);
  }

void Fix::setVelocity(float* ned_velocity)
  {
    for (int i = 0; i < 3; i++)
      {
        uint16_t vel16 = Data::getFloat16(ned_velocity[i]);
        Data::setData((void*) &vel16, 256 + i * 16, 16);
      }
  }

void Fix::setGPSInfo(uint8_t sats_used, STATUS status, float pdop)
  {
    Data::setData((void*) &sats_used, 304, 6);
    Data::setData((void*) &status, 310, 2);
    uint16_t pdop16 = Data::getFloat16(pdop);
    Data::setData((void*) &pdop16, 312, 16);
  }

void Fix::setPositionCovariance(float* positionCovariance,
    uint8_t sizePositionCovariance)
  {
    for (int i = 0; i < sizePositionCovariance; i++)
      {
        uint16_t posVar16 = Data::getFloat16(positionCovariance[i]);
        Data::setData((void*) &posVar16, 332 + (i * 16), 16);
      }
  }

void Fix::setVelocityCovariance(float* velocityCovariance,
    uint8_t sizeVelocityCovariance, uint8_t sizePositionCovariance)
  {
    for (int i = 0; i < sizeVelocityCovariance; i++)
      {
        uint16_t velVar16 = Data::getFloat16(velocityCovariance[i]);
        Data::setData((void*) &velVar16,
            332 + (sizePositionCovariance * 16) + (i * 16), 16);
      }
  }

Fix::~Fix()
  {
//	printf("Delete Fix!\n");
  }

} /* namespace uavcan */
