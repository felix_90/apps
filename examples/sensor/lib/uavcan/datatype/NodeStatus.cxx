/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Auxiliary.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/datatype/NodeStatus.h"

namespace uavcan {

NodeStatus::NodeStatus(node_status_s &node_data)
    : Data(7, SIGNATURE, SERVICE, DATATYPEID)
  {
	setUptime(node_data.time);
	setHealth(node_data.health);
	setMode(node_data.mode);
	setSubMode(node_data.subMode);
	setVendorSpecificStatus(node_data.vendorStatus);

  }

void NodeStatus::setUptime(uint32_t time) {
	Data::setData((void*)&time, 0, 32);
}
void NodeStatus::setHealth(uint8_t health) {
	Data::setData((void*)&health, 32, 2);
}
void NodeStatus::setMode(uint8_t mode) {
	Data::setData((void*)&mode, 34, 3);
}
void NodeStatus::setSubMode(uint8_t subMode) {
	Data::setData((void*)&subMode, 37, 3);
}
void NodeStatus::setVendorSpecificStatus(uint16_t status) {
	Data::setData((void*)&status, 40, 16);
}




NodeStatus::~NodeStatus()
  {
//	printf("Delete MagnetFieldObject!\n");
  }

} /* namespace uavcan */
