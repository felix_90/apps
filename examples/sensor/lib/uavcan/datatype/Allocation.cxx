/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Allocation.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/datatype/Allocation.h"

namespace uavcan {


Allocation::Allocation(allocation_s &alloc_data)
    : Data(1 + alloc_data.unique_id_size, SIGNATURE, SERVICE, DATATYPEID)
  {
	size = 1 + alloc_data.unique_id_size;
    setNodeId(alloc_data.node_id);
    setFirstPart(alloc_data.first_part_of_unique_id);
    setUniqueId(alloc_data.unique_id, alloc_data.unique_id_size);

  }

void Allocation::setNodeId(uint8_t id)
  {
    Data::setData((void*) &id, 0, 7);
  }

void Allocation::setFirstPart(bool isFirstPart)
  {
    uint8_t isFirst = isFirstPart ? 0x01 : 0x00;
    Data::setData((void*) &isFirst, 7, 1);
  }

void Allocation::setUniqueId(uint8_t* unique_id, uint8_t idSize)
  {
    Data::setData((void*) unique_id, 8, idSize * 8);
  }

Allocation::~Allocation()
  {
  }

} /* namespace uavcan */
