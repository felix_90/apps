/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/StaticTemperature.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_STATICTEMPERATURE_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_STATICTEMPERATURE_H_

#include "../../../../sensor/lib/uavcan/datatype/Data.h"

namespace uavcan {

struct temperature_s {
  float temperature;
  float variance;
};

class StaticTemperature: public Data {
  static const uint16_t DATATYPEID = 1029;
  static const uint64_t SIGNATURE = 0x49272a6477d96271UL;
  static const bool SERVICE = false;

  void setTemperature(float temperature);
  void setVariance(float variance);

public:
  static uint16_t getDatatype()
    {
      return DATATYPEID;
    }
  StaticTemperature(temperature_s &temp_data);

  virtual ~StaticTemperature();
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_STATICTEMPERATURE_H_ */
