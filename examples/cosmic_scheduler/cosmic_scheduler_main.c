/****************************************************************************
 * examples/cosmic_scheduler/cosmic_scheduler.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <mqueue.h>
#include <nuttx/timers/timer.h>

#include <semaphore.h>
#include <mqueue.h>

#include <nuttx/timers/hptc.h>
/*IMU*/
#include <nuttx/sensors/adis16xxx.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

//#define CONFIG_COSMIC_SCHEDULER_DEVNAME "/dev/timer12"
//#define CONFIG_COSMIC_SCHEDULER_INTERVAL 10000
//#define CONFIG_COSMIC_SCHEDULER_SIGNO 17
//#define CONFIG_COSMIC_SCHEDULER_DELAY 10000

//#define CHILD_ARG ((void*)0x12345678)

//int child_apid;

#ifdef CONFIG_EXAMPLES_SENSOR
#define UAVCAN_START 		0x00
#define UAVCAN_SEND 		0x01
#define UAVCAN_STOP 		0x02
#endif

#ifdef CONFIG_EXAMPLES_SENSOR
struct uavcanData_s{
	uint64_t timestamp;
	int32_t height_mm;
	int64_t longitude_1e8;
	int64_t latitude_1e8;
	float velocity[3];
	float yaw_degree;
	float pitch_degree;
	float roll_degree;
};

static mqd_t mqd_uavcan;

#endif




/****************************************************************************
 * Public Functions
 ****************************************************************************/
//static volatile unsigned long g_nsignals;


/****************************************************************************
 * cosmic_scheduler_main
 ****************************************************************************/



#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int cosmic_scheduler_main(int argc, char *argv[])
#endif
{
#if 0
  struct timer_notify_s notify;
  struct sigaction act;
  struct timer_status_s status;
#endif
  int ret;
  int fd;
  sem_t* sem_hptc;
  sem_t* sem_nutros_tx;

  mqd_t mqd_imu;
  struct mq_attr mq_imu_attr;




  //sigo =17;

//  int fd_mx28;

  int fd_imu;
  struct adis16488_msg_s imu_data;
  FAR struct timespec imu_timestamp;

//  pthread_attr_t attr;
//  pthread_t thread;


//TODO: uncomment nutros_a_main();
//  nutros_a_main();



  /* Open the timer device */

  printf("Open %s\n", CONFIG_HPTC_DEV_PATH);

  fd = open(CONFIG_HPTC_DEV_PATH, O_RDONLY);
  if (fd < 0)
    {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  CONFIG_HPTC_DEV_PATH, errno);
      return EXIT_FAILURE;
    }



    printf("get hptc semaphore\n");

    ret = ioctl(fd, HPTCIOC_GETSEM, (long int)&sem_hptc);
    if (ret < 0)
      {
        fprintf(stderr, "ERROR: Failed to get hptc semaphore: %d\n", errno);
        close(fd);
        return EXIT_FAILURE;
      }

//    int sem_val=0;
//    sem_getvalue(sem_hptc,&sem_val);
//    printf("sem: val=%d\n",sem_val);

    //------------------------------------------------------------------------------------------


     /* Start the timer */

     printf("Start the timer\n");

     ret = ioctl(fd, HPTCIOC_START, 0);
     if (ret < 0)
       {
         fprintf(stderr, "ERROR: Failed to start the hptc: %d\n", errno);
         close(fd);
         return EXIT_FAILURE;
       }





     mq_imu_attr.mq_maxmsg  = 2;
     mq_imu_attr.mq_msgsize = sizeof(struct adis16488_msg_s);
     mq_imu_attr.mq_flags   = 0;

     printf("create mq_imu, msg_size: %d\n",sizeof(struct adis16488_msg_s) );

     mqd_imu = mq_open("mq_imu",O_CREAT | O_WRONLY | O_NONBLOCK, 0666, &mq_imu_attr);

	 if(mqd_imu < 0){
		 printf("creating mqd_imu failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_imu created: %i \n",mqd_imu);
	 }
//     sleep(1);


#ifdef CONFIG_EXAMPLES_SENSOR
	 printf("run uav-can sensor_main\n");
	 sleep(1);
     sensor_main();
#if 0
	 mqd_uavcan = mq_open("mq_uavcan",O_NONBLOCK | O_WRONLY);
	 if((int)mqd_uavcan < 0){
		 printf("mqd_uavcan failed \n");
	 }else{
		 printf("mqd_uavcan success: %i \n",mqd_uavcan);
	 }
#endif


#endif







    printf("open nutros_tx semaphore...\n");

     sem_nutros_tx = sem_open("nutros_tx",O_CREAT,0,0);
     printf("ret sem_nutros_tx: %i \n", sem_nutros_tx);
     /*warten*/
//     sleep(1);

     ret = sem_setprotocol(sem_nutros_tx, SEM_PRIO_NONE);
      if (ret < 0)
        {
           printf("ERROR: sem_setprotocol failed: %d\n", ret);
           return EXIT_FAILURE;
        }

#if 1
//     printf("run nutros_a_main(), waiting for USB connection...\n");
//     sleep(1);
     nutros_a_main();
     printf("Nutros running.\n");
//     sleep(1);
#endif


#if 1
     /*open imu*/
     printf("Open imu\n");
     fd_imu = open("/dev/imu0",O_RDWR);
     if(fd_imu<0){
       printf("error opening /dev/imu0\n");
     }

     printf("imu opened\n");
//     sleep(2);
#endif









 printf("run...\n");
 //sleep(2);

#if 0
     for(int i=0; i<10;i++){
    	 struct hptc_status_s hptc_status;
    	 ret = ioctl(fd, HPTCIOC_GETSTATUS, (long int) &hptc_status);
    	 printf("%2d:cnt:%10u; cnt1:%10u; cnt2:%10u\n", i,hptc_status.flags,hptc_status.cnt1,hptc_status.cnt2);
    	 sleep(1);
     }
#endif


     bool slice=true;
     int i=0;
//     int j=0;
     int iax=0;
     int iay=0;
     int iaz=0;

     for(;;){
    	 sem_wait(sem_hptc); //2khz hardcoded in hptc at time of writing this file

    	 if(slice){

    		 if(fd_imu>=0){
    		 read(fd_imu,&imu_data,sizeof(imu_data));
   			 clock_gettime(CLOCK_REALTIME, &imu_timestamp);
   			 //timestamp workaround:
   			 imu_data.timestamp_s=(int32_t)imu_timestamp.tv_sec;
   			 imu_data.timestamp_ns=(int32_t)imu_timestamp.tv_nsec;

   			 mq_send(mqd_imu,(char*)&imu_data,sizeof(imu_data),1);

    		 }

   			 //dynamixel read request
   			 //read pps timestamp, control time

    		 slice=false;
    	 } else {
    		 //dynamixel read rx-buffer
    		 //dynamixel set speed/pos if required
    		 //read lidar timestamp

#if 0
if (i==999){
	i=0;

	iax=imu_data.x_accl;
	iay=imu_data.y_accl;
	iaz=imu_data.z_accl;


	printf("Time: %d.%09ds\n",imu_timestamp.tv_sec,imu_timestamp.tv_nsec);
	printf("%4d ax: %8.4lf ay: %8.4lf az: %8.4lf\n",i, 0.122e-7*(double)iax, 0.122e-7*(double)iay, 0.122e-7*(double)iaz);


	//printf("mq_send: %d\n", ret);
} else {
	i++;
}
#endif

    		 //trigger_nutros_tx

    		    int sem_val=0;
    		    sem_getvalue(sem_nutros_tx,&sem_val);
    		    if(sem_val<1)sem_post(sem_nutros_tx);

    		    slice=true;
    	 }


     }
  //never reached
  return 0;
}
