/****************************************************************************
 * examples/vamexrotation/vamexrotation_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <nuttx/sensors/qencoder.h>

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * task 1
 ****************************************************************************/


static int task1_main(int argc, char *argv[])
{
	int i;
	int ret;
	int fd;
	int fd_qe[4];
	int32_t position[4];

	printf("task started\n");

	/*Open all Drivers*/

	fd = open("/dev/qe1", O_RDONLY);

	  if (fd < 0)
	    {
		  printf("qe_main: open /dev/qe1 failed: %d\n", errno);
	    }else{
	    	printf("open /dev/qe1 successful\n");
	    }

	fd_qe[0] = fd;

	fd = open("/dev/qe2", O_RDONLY);

	  if (fd < 0)
		{
		  printf("qe_main: open /dev/qe2 failed: %d\n", errno);
		}else{
	    	printf("open /dev/qe2 successful\n");
	    }

	fd_qe[1] = fd;

	fd = open("/dev/qe3", O_RDONLY);
	  if (fd < 0)
		{
		  printf("qe_main: open /dev/qe3 failed: %d\n", errno);
		}else{
	    	printf("open /dev/qe3 successful\n");
	    }

	fd_qe[2] = fd;

	fd = open("/dev/qe4", O_RDONLY);

	  if (fd < 0)
		{
		  printf("qe_main: open /dev/qe4 failed: %d\n", errno);
		}else{
	    	printf("open /dev/qe4 successful\n");
	    }

	fd_qe[3] = fd;

	printf("driver successfully loaded\n");


	/*loop forever, to get the positions*/

	for(;;){
	  ret = ioctl(fd_qe[i], QEIOC_POSITION, (unsigned long)((uintptr_t)&position[i]));

	  if (ret < 0)
		{
		  printf("qeadc_main: ioctl(QEIOC_POSITION) failed: %d\n", errno);
		}

	  printf("encoder %x: %6hi;   ",i,(int16_t)(position[i]));


	  i++;
	  usleep(10000);
	  if(i==4){
		  printf("\n---------------------------------------------------------------------------------------------\n");
		  i=0;
		  usleep(1000000);
	  }
	  }


	return 0;
}

/****************************************************************************
 * vamexrotation_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int vamexrotation_main(int argc, char *argv[])
#endif
{
	int ret;
	/*create Task*/
	ret = task_create("Task_1", 1, 1024, task1_main, NULL);

		if (ret < 0)
			{
			  printf("Main: ERROR Failed to start task 1\n");
			}
		  else
			{
			  printf("Main: Started task1_main at PID=%d\n", ret);
			}

  return 0;
}
